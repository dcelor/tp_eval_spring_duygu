Questions

Partie Docker
-------------------------------------------------------------------------------

1-Qu'est ce qu'un conteneur ? Quelle différence entre image et conteneur ?

Un conteneur Docker est un environnement d'exécution virtualisé dans 
lequel les utilisateurs peuvent isoler des applications.

Une image Docker est un fichier immuable qui contient le code source, 
les bibliothèques, les dépendances, les outils et les autres fichiers 
nécessaires à l'exécution d'une application.

Les images peuvent exister sans conteneurs, alors qu'un conteneur doit exécuter 
une image pour exister. Par conséquent, les conteneurs dépendent des images et 
les utilisent pour créer un environnement d'exécution et exécuter une application.

2-A quoi sert "docker run", "docker build "?

La commande docker run crée un conteneur à partir d'une image et démarre le 
conteneur.

La commande docker build crée des images Docker à partir d'un Dockerfile et 
d'un contexte. Le contexte  est l'ensemble des fichiers situés dans le chemin ou 
l'URL spécifié.


3-Quels sont les avantages d'utiliser un système comme Docker :
    dans le cadre du développement d'une application ?
    dans le cadre du test d'une application ?
    dans le cadre du run / exploitation en production d'une application ?

Avec Docker l’environnement sera identique sur n’importe 
quelle plateforme, ce qui fait gagner un temps précieux dans les projets de 
développements du test et exploitation en production. Il donne rapidité et 
facilité de déploiement des applications

Contrairement à un serveur virtuel sous Linux, le conteneur n’a besoin pas 
beaucoup de memoire. 


4-Quelle est la commande à taper pour construire une image Docker à partir du
Dockerfile créé plus haut ?

docker build

Partie intégration continue et tests
--------------------------------------------------------------------------------

1-Citez quelques intérêts d'avoir une intégration continue dans le cadre d'un
projet de développement

Amélioration de l'adhérence du code.
Détection des erreurs et bugs très rapidement et réduire le délai pour les corriger.
La modification est testée immédiatement.

2-Quelle différence entre un test d'intégration et un test fonctionnel ?

Les tests d'intégration sont par définition tout test qui teste plusieurs composants. 
Il peut s'agir d'un niveau bas pour tester deux classes qui fonctionnent ensemble, 
ou il peut s'agir de tests qui testent l'ensemble du système dans son ensemble.

Le test fonctionnel consiste à tester le système par rapport aux exigences 
fonctionnelles du produit.


3-Quel intérêt d'utiliser Docker dans le cadre de tests automatisés ?

Il est plus facile d'effectuer les tests. Les phases de productions sont simplifiées.
