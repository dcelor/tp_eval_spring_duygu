CREATE TABLE writer (id int IDENTITY,
name varchar(256) not null,
PRIMARY KEY (id)
);
CREATE TABLE category (id int IDENTITY,
name varchar(256) not null,
color varchar(256) not null,
PRIMARY KEY (id)
);

CREATE TABLE article (id int IDENTITY,
title varchar(256) not null,
content varchar(256) not null,
id_writer int ,
id_category int ,
PRIMARY KEY (id),
);

ALTER TABLE article
ADD FOREIGN KEY (id_writer) REFERENCES writer(id)

ALTER TABLE article
ADD FOREIGN KEY (id_category) REFERENCES category(id)