package com.example.duygu.tables;

import javax.persistence.*;


@Entity
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String title;
    private String content;


    @ManyToOne
    @JoinColumn(name = "id_writer")
    private Writer writer;


    @ManyToOne
    @JoinColumn(name = "id_category")
     private Category category;

    public Article() {
    }

    public Article(String title, String content, Writer writer, Category category) {
        this.title = title;
        this.content = content;
        this.writer = writer;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Writer getWriter() {
        return writer;
    }

    public void setWriter(Writer writer) {
        this.writer = writer;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
