package com.example.duygu.tables;

import javax.persistence.*;


public class ArticleInt {

    private String title;
    private String content;
    private Integer id_writer;
    private Integer id_category;


    public ArticleInt() {
    }

    public ArticleInt(String title, String content, Integer id_writer, Integer id_category) {
        this.title = title;
        this.content = content;
        this.id_writer = id_writer;
        this.id_category = id_category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getId_writer() {
        return id_writer;
    }

    public void setId_writer(Integer id_writer) {
        this.id_writer = id_writer;
    }

    public Integer getId_category() {
        return id_category;
    }

    public void setId_category(Integer id_category) {
        this.id_category = id_category;
    }
}
