package com.example.duygu.controllers;

import com.example.duygu.repos.CategoryRepository;;
import com.example.duygu.tables.Category;
import com.example.duygu.tables.Writer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/category")
public class CategoryController {


    private Category category;
    private int id;

    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping
    public List<Category> listCategories(){
        return categoryRepository.findAll();
    }

    @PostMapping
    public Category addCategory(@RequestBody Category category) {
        categoryRepository.save(category);
        return category;
    }


    @DeleteMapping("/{id}")
    public Category deleteCategory(@PathVariable("id") int id) {
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if(optionalCategory.isPresent()){
            categoryRepository.deleteById(id);
            return optionalCategory.get();
        }else{
            return null;
        }
    }
}
