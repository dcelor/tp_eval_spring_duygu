package com.example.duygu.controllers;

import com.example.duygu.repos.ArticleRepository;
import com.example.duygu.repos.CategoryRepository;
import com.example.duygu.repos.WriterRepository;
import com.example.duygu.tables.Article;
import com.example.duygu.tables.ArticleInt;
import com.example.duygu.tables.Category;

import com.example.duygu.tables.Writer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/article")
public class ArticleController {


    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private WriterRepository writerRepository;

    @GetMapping
    public List<Article> listArticles(){
        return articleRepository.findAll();
    }


    @GetMapping("/{id}")
    public Article getArticleById(@PathVariable("id") int id) {
        Optional<Article> optionalArticle = articleRepository.findById(id);
        if(optionalArticle.isPresent()){
            return optionalArticle.get();
        }else{
            return null;
        }
    }


    @GetMapping("/category/{id}")
    public List<Article> getArticlesByCategory(@PathVariable("id") int id) {
        Optional<Category> categoryOptional = categoryRepository.findById(id);
        if(categoryOptional.isPresent()) {
            List<Article> articles = new ArrayList<>();
            for (Article article : listArticles()) {
                if (article.getCategory().getId() == id) {
                    articles.add(article);
                }
            }
            return articles;
        }
        return null;
    }

    @PostMapping
    public Article addArticle(@RequestBody ArticleInt articleInt) {
        Article article = new Article();
        if(articleInt.getId_category()!= null) {
            Optional<Category> categoryOptional = categoryRepository.findById(articleInt.getId_category());
            if(categoryOptional.isPresent()) {
                article.setCategory(categoryOptional.get());
            }
        }
        if(articleInt.getId_writer()!= null) {
            Optional<Writer> writerOptional = writerRepository.findById(articleInt.getId_writer());
            if(writerOptional.isPresent()) {
                article.setWriter(writerOptional.get());
            }
        }


        article.setContent(articleInt.getContent());
        article.setTitle(articleInt.getTitle());
        articleRepository.save(article);
        return article;
    }


    @DeleteMapping("/{id}")
    public Article deleteArticle(@PathVariable("id") int id) {
        Optional<Article> optionalArticle = articleRepository.findById(id);
        if(optionalArticle.isPresent()){
            articleRepository.deleteById(id);
            return optionalArticle.get();
        }else{
            return null;
        }
    }
}
