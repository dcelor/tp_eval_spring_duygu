package com.example.duygu.controllers;

import com.example.duygu.repos.WriterRepository;
import com.example.duygu.tables.Writer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/writer")
public class WriterController {

    private Writer writer;
    private int id;

    @Autowired
    private WriterRepository writerRepository;

    @GetMapping
    public List<Writer> listWriters(){
        return writerRepository.findAll();
    }

    @GetMapping("/{id}")
    public Writer getWriterById(@PathVariable("id") int id) {
        Optional<Writer> optionalWriter = writerRepository.findById(id);
        if(optionalWriter.isPresent()){
            return optionalWriter.get();
        }else{
            return null;
        }
    }
    @PostMapping
    public Writer addWriter(@RequestBody Writer writer) {
        writerRepository.save(writer);
        return writer;
    }

    @PutMapping("/{id}")
    public Writer editWriter(@RequestBody Writer writer, @PathVariable("id") int id) {
        return writer;
    }

    @DeleteMapping("/{id}")
    public Writer deleteWriter(@PathVariable("id") int id) {
        Optional<Writer> optionalWriter = writerRepository.findById(id);
        if(optionalWriter.isPresent()){
            writerRepository.deleteById(id);
            return optionalWriter.get();
        }else{
            return null;
        }
    }
}

