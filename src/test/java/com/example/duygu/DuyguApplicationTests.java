package com.example.duygu;

import com.example.duygu.repos.ArticleRepository;
import com.example.duygu.repos.CategoryRepository;
import com.example.duygu.repos.WriterRepository;
import com.example.duygu.tables.Article;
import com.example.duygu.tables.Category;
import com.example.duygu.tables.Writer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class DuyguApplicationTests {


	@Autowired private MockMvc mvc;
	@Autowired WriterRepository writerRepository;
	int writerId;
	@Autowired
	CategoryRepository categoryRepository;
	int categoryId;
	@Autowired
	ArticleRepository articleRepository;
	int articleId;


	@Before
	public void setUp() {
		Writer writer = new Writer();
		writer.setName("Duygu Celor");
		writer = writerRepository.save(writer);
		writerId = writer.getId();

		Category category = new Category();
		category.setName("Science");
		category.setColor("blue");
		category = categoryRepository.save(category);
		categoryId = writer.getId();

		Article article = new Article();
		article.setTitle("Life on Mars");
		article.setContent("Ble ble ble bla");
		article.setWriter(writer);
		article.setCategory(category);
		article = articleRepository.save(article);
		articleId = article.getId();

	}

	@Test
	public void postWriter() throws Exception {
		mvc.perform(
				MockMvcRequestBuilders.post("/writer")
						.contentType(MediaType.APPLICATION_JSON)
						.content("{\"name\":\"Duygu\"}"))
				.andExpect(status().is2xxSuccessful());

		Assert.assertEquals(writerRepository.count(), 2);
	}

	@Test
	public void postCategory() throws Exception {
		mvc.perform(
				MockMvcRequestBuilders.post("/category")
						.contentType(MediaType.APPLICATION_JSON)
						.content("{\"name\":\"Art\" , \"color\":\"pink\"}"))
				.andExpect(status().is2xxSuccessful());

		Assert.assertEquals(categoryRepository.count(), 2);
	}

	@Test
	public void postArticle() throws Exception {
		mvc.perform(
				MockMvcRequestBuilders.post("/article")
						.contentType(MediaType.APPLICATION_JSON)
						.content("{\"title\":\"Evolution of music\" , \"content\":\"in France\" , \"id_writer\":\"0\" , \"id_category\":\"0\" }"))
				.andExpect(status().is2xxSuccessful());

		Assert.assertEquals(articleRepository.count(), 2);
	}

	@Test
	public void getArticles() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/article"))
				.andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].title").value("Life on Mars"));
	}

	@Test
	public void getArticle() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/article/" + articleId))
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").value(articleId))
				.andExpect(MockMvcResultMatchers.jsonPath("$.title").value("Life on Mars"));
	}

	@Test
	public void deleteArticle() throws Exception {
		mvc.perform(
				MockMvcRequestBuilders.delete("/article/" + articleId));

		Assert.assertEquals(articleRepository.count(), 1);
		//Assert.assertFalse(articleRepository.findById(articleId).isPresent());
	}



}
