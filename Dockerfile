FROM openjdk:8-slim
COPY target/duygu-0.0.1-SNAPSHOT.jar /app.jar
ENTRYPOINT ["java","-Dava.security.egd=file:/dev/./urandom","-jar","/app.jar"]j